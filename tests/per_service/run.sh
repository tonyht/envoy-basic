#!/bin/bash

ENDPOINTS="
GET http://localhost:8010/service-foo/public/0\n
GET http://localhost:8010/service-foo/public/1\n
GET http://localhost:8010/service-foo/public/2\n
GET http://localhost:8010/service-foo/public/3\n
GET http://localhost:8010/service-foo/public/4\n
GET http://localhost:8010/service-foo/public/5\n
GET http://localhost:8010/service-foo/public/6\n
GET http://localhost:8010/service-foo/public/7\n
GET http://localhost:8010/service-foo/public/8\n
GET http://localhost:8010/service-foo/public/9
"

echo 'testing 400% rate limit ...'
echo -e $ENDPOINTS | vegeta attack -rate=200 -duration=10s | tee results.bin | vegeta report
