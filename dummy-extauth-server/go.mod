module gitlab.myteksi.net/phuong.huynh/envoy-ratelimit-experiment

go 1.14

require (
	github.com/envoyproxy/go-control-plane v0.9.5
	github.com/gogo/googleapis v1.3.2
	google.golang.org/genproto v0.0.0-20200410110633-0848e9f44c36
	google.golang.org/grpc v1.28.1
)
